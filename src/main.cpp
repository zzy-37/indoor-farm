#include "main.h"

#define LED_PIN 3

#define KNOB_CLK_PIN 4
#define KNOB_DT_PIN 5
#define KNOB_SW_PIN 6

#define PUMP_PIN 7
#define AUTOMIZER_PIN 8

#define LIGHT_SENSOR_PIN A0
#define WATER_SENSOR_PIN A1

#define LIGHT_THRESHOLD 600

#define AUTOMIZER_DURATION 3000

#define ON 0
#define OFF 1

LiquidCrystal_I2C lcd(0x27, 20, 4);

void (*knob_handler)(bool clockwise) = NULL;

unsigned automizer_gap = 0;
bool automizer_state = OFF;

uint8_t max_brightness = 255;
bool led_state = OFF;

unsigned long loop_time = 0;
uint8_t menu_pointer = 0;

void lcd_display()
{
  static unsigned long last_lcd_update_time = 0;
  if (loop_time > last_lcd_update_time + 100)
  {
    lcd.clear();

    last_lcd_update_time = loop_time;
  }
}

void setup()
{
  pinMode(PUMP_PIN, OUTPUT);
  pinMode(KNOB_SW_PIN, INPUT_PULLUP);

  lcd.init();
  lcd.backlight();

  Serial.begin(9600);
}

void loop()
{
  loop_time = millis();

  int water_val = analogRead(WATER_SENSOR_PIN);

  static bool last_knob_sw_state = HIGH;
  bool knob_sw_state = digitalRead(KNOB_SW_PIN);
  bool knob_pressed = last_knob_sw_state == HIGH && knob_sw_state == LOW;
  last_knob_sw_state = knob_sw_state;

  static bool last_knob_clk_state;
  bool knob_clk_state = digitalRead(KNOB_SW_PIN);
  if (knob_clk_state != last_knob_clk_state)
  {
    if (knob_handler)
      knob_handler(digitalRead(KNOB_DT_PIN) != knob_clk_state);
  }
  last_knob_clk_state = knob_clk_state;

  int light_val = analogRead(LIGHT_SENSOR_PIN);
  if (light_val < LIGHT_THRESHOLD)
  {
    uint8_t brightness = map(light_val, LIGHT_THRESHOLD, 0, 0, max_brightness);
    analogWrite(LED_PIN, 255 - brightness);
  }

  static unsigned long last_saved_time = 0;
  static bool automizer_state = OFF;
  if (automizer_state == ON)
  {
    if (loop_time > last_saved_time + AUTOMIZER_DURATION)
    {
      digitalWrite(AUTOMIZER_PIN, OFF);
      last_saved_time = loop_time;
    }
  }
  else if (loop_time > last_saved_time + automizer_gap * 1000)
  {
    digitalWrite(AUTOMIZER_PIN, ON);
    last_saved_time = loop_time;
  }
}