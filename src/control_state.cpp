#include "main.h"

struct Control_State
{
    virtual void on_display(void) {}
    virtual void on_knob_turn(bool clockwise) {}
    virtual void on_knob_press(void) {}
};

#define MENU(menu) Menu_Contorl(menu, sizeof(menu) / sizeof(menu[0]))

struct Menu_Contorl : Control_State
{
    Menu_Contorl(Menu_Item *menu, uint8_t count)
        : m_menu(menu), m_menu_count(count) {}

    void on_display(void) override
    {
        lcd.setCursor(1, 0);
        if (m_menu[m_display_ptr].print)
            m_menu[m_display_ptr].print();

        if (m_display_ptr + 1 < m_menu_count)
        {
            lcd.setCursor(1, 1);
            if (m_menu[m_display_ptr + 1].print)
                m_menu[m_display_ptr + 1].print();
        }

        lcd.setCursor(0, m_display_ptr == m_menu_ptr ? 0 : 1);
        lcd.print(">");
    }

    void on_knob_turn(bool clockwise) override
    {
        if (clockwise)
        {
            if (m_menu_ptr + 1 < m_menu_count)
                ++m_menu_ptr;
            if (m_menu_ptr > m_display_ptr + 1)
                m_display_ptr = m_menu_ptr - 1;
        }
        else
        {
            if (m_menu_ptr > 0)
                --m_menu_ptr;
            if (m_menu_ptr < m_display_ptr)
                m_display_ptr = m_menu_ptr;
        }
    }

    void on_knob_press(void) override
    {
        if (m_menu[m_menu_ptr].on_press)
            m_menu[m_menu_ptr].on_press();
    }

private:
    Menu_Item *m_menu;
    uint8_t m_menu_count;

    uint8_t m_menu_ptr = 0;
    uint8_t m_display_ptr = 0;
};

struct Brightness_Control : Control_State
{
    void on_display() override
    {
        lcd.setCursor(0, 0);
        lcd.print("Max Brightness: ");
        lcd.setCursor(0, 1);
        lcd.print(" 0-255  -- ");
        lcd.print(max_brightness);
    };

    void on_knob_turn(bool clockwise) override
    {
        if (clockwise)
        {
            if (max_brightness < 255)
                ++max_brightness;
        }
        else if (max_brightness > 0)
            --max_brightness;
    };

    void on_knob_press() override
    {
        current_control = &main_menu;
    };
};

struct Automizer_Setting : Control_State
{
    void on_display() override
    {
        lcd.print("");
    }

    void on_knob_turn(bool clockwise) override
    {
    }
};

struct Info_Contorl : Control_State
{
    void on_display() override {}
    void on_knob_turn(bool clockwise) override {}
    void on_knob_press() override {}
};

struct Menu_Item
{
    void (*print)(void);
    void (*on_press)(void);
};

Control_State *current_control = NULL;

Menu_Contorl main_menu = MENU(home);
Menu_Item home[] = {
    {.print = []()
     { lcd.print("Info"); },
     .on_press = []()
     { current_control = &info; }},

    {.print = []()
     { lcd.print("Light: ");
       lcd.print(is_lighting ? "On" : "Off"); },
     .on_press = []()
     { is_lighting = !is_lighting; }},

    {.print = []()
     { lcd.print("Set Brightness"); },
     .on_press = []()
     { current_control = &brightness_setting; }},

    {.print = []()
     { lcd.print("Automizer: ");
       lcd.print(is_lighting ? "On" : "Off"); },
     .on_press = []()
     { is_lighting = !is_lighting; }},
};

Info_Contorl info;
Brightness_Control brightness_setting;

Menu_Item menu_set_brightness = {
    .print = []()
    { lcd.print("Set brightness"); },
    .on_press = []()
    { current_control = &brightness_setting; }};

Menu_Item menu_toggle_light = {
    .print = []()
    {
        lcd.print("Light: ");
        lcd.print(is_lighting ? "On" : "Off"); },
    .on_press = []()
    { is_lighting = !is_lighting; }};

Menu_Item menu_go_back = {
    .print = []()
    { lcd.print("Go back"); },
    .on_press = []()
    { current_control = &main_menu; }};