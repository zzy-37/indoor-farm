#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

extern LiquidCrystal_I2C lcd;
extern void (*knob_handler)(bool clockwise);
extern uint8_t max_brightness;
extern bool is_lighting;